﻿using System;


namespace classes_homework
{

    internal class Program
    {
        static void Main(string[] args)
        {
            var cal = new Calculator(1,2,3,4,5);
            var subtract = cal.Subtract();
            var add = cal.Add();
            var multiply = cal.Multiply();

            Console.WriteLine($" subtract : { subtract}");
            Console.WriteLine("--------------------");
            Console.WriteLine($" Add : {add}");
            Console.WriteLine("--------------------");
            Console.WriteLine($" Multiply : {multiply}");
            Console.WriteLine("--------------------");

        }
    }
}
