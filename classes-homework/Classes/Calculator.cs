﻿using System;
using System.Linq;
namespace classes_homework
{
    public class Calculator : CalculatorBase
    {
        private int[] NumArray { get; set; }
        public Calculator(params int[] arguments)
        {
            this.NumArray = arguments;
        }
        public override int Subtract()
        {
            return NumArray.Aggregate((a, b) => a - b);
        }
        public override int Add()
        {
            return NumArray.Sum();
        }
        public override int Multiply()
        {
            return NumArray.Aggregate((a, b) => a * b);
        }
    }
}
