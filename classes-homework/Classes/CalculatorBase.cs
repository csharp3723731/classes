﻿namespace classes_homework
{
    public abstract class CalculatorBase
    {
        public abstract int Subtract();
        public abstract int Add();
        public abstract int Multiply();


    }
}
